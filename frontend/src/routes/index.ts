import Login from "../views/login.svelte"
import Chat from "../views/chat.svelte"
import Filemanage from "../views/filemanage.svelte"
import Usermanage from "../views/usermanage.svelte"
import Logviewrouter from "../modules/logview/router/log-view.route"
import { wrap } from "svelte-spa-router/wrap"
const router = {
    "/": Login,
    "/choose": wrap({
        asyncComponent: () => import('../views/choose.svelte')
    }),
    "/chat": Chat,
    "/chat/:first/:second": Chat,
    "/choosepepole": wrap({
        asyncComponent: () => import('../views/choosepeole.svelte')
    }),
    "/filemanage": Filemanage,
    "/usermanage": Usermanage,
    ...Logviewrouter
}
export default router;