package chat

//author: {"name":"auth","email":"XUnion@GMail.com"}
//annotation:chat-service

import (
	"context"
	"desktop/cmn"
	"desktop/serve/login"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

func Enroll(author string) {
	var developer *cmn.ModuleAuthor
	if author != "" {
		var d cmn.ModuleAuthor
		err := json.Unmarshal([]byte(author), &d)
		if err != nil {
			log.Println(err.Error())
			return
		}
		developer = &d
	}

	cmn.AddService(&cmn.ServeEndPoint{
		Fn: chat,

		Path: "/api/chat",
		Name: "chat",

		Developer: developer,
	})
	cmn.AddService(&cmn.ServeEndPoint{
		Fn: getUid,

		Path: "/api/getUid",
		Name: "getUid",

		Developer: developer,
	})
}

var UP = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type MessageType struct {
	Message string `json:"message"`
	Sender string `json:"sender"`
	Receiver string `json:"receiver"`
}
//
type ConnsType struct {
	Conns *websocket.Conn
	Sender string
	Receiver string
}
var conns []ConnsType

// authMgmt authenticate/authorization management
func chat(ctx context.Context) {
	q := cmn.GetCtxValue(ctx)
	fmt.Println("聊天服务开始时间：",q.BeginTime)
	fmt.Println("聊天")
	fmt.Println("url:",q.R.URL)
	sender := q.R.URL.Query().Get("sender")
	receiver := q.R.URL.Query().Get("receiver")
	if sender==""&&receiver=="" {
		fmt.Println("sender or receiver not define")
		return
	}
	conn,err :=UP.Upgrade(q.W,q.R,nil)
	if err!=nil {
		log.Println(err)
		return
	}
	var Conn ConnsType
	var senders string
	senders = sender
	Conn.Conns = conn
	Conn.Receiver = receiver
	Conn.Sender = senders
	exist := 0
	for _,v :=range conns{
		if v==Conn{
			exist = 1
		}
	}
	if exist!=1 {
		conns = append(conns,Conn)
	}
	fmt.Println("coons:",conns)
	if err==nil{
		fmt.Println("connect successfully")
	}
	for {
		//读取消息
		_, p, e :=conn.ReadMessage()
		if e!=nil {
			break
		}
		fmt.Println("p",string(p))
		var Data MessageType
		err =json.Unmarshal(p,&Data)
		if err!=nil {
			fmt.Println("err",err)
		}
		fmt.Println(Data)
		for key,value :=range conns{
			fmt.Println("key",key)
			fmt.Println("value",value)
			if Conn.Receiver==value.Sender {
				conns[key].Conns.WriteMessage(1,[]byte(fmt.Sprintf(`{"message":"%s","senderid":"%s"}`,Data.Message,Conn.Sender)))
			}
		}
	}
	defer conn.Close()
	log.Println("关闭服务")
}

func getUid(ctx context.Context){
	q := cmn.GetCtxValue(ctx)
	session, _ := login.Store.Get(q.R, "session-name")
	uid := session.Values["userid"]
	q.W.Write([]byte(fmt.Sprintf(`{"uid":%d}`,uid)))
}